## Oope2 harjoitustyö: Tiedonhallintaohjelma.

Tämä työhakemisto sisältää harjoitusprojektin yksinkertaisen tiedonhallintaohjelman kehittämiseen. 
Ohjelmassa on tekstipohjainen käyttöliittymä, jonka avulla voi luoda tai käsitellä **hakemistoja** ja **tiedostoja**.
Komentoja voi käyttää esimerkiksi hakemistojen selaamisen tai tietojen uudelleen nimeämiseen.

Copy of my project for an object oriented programming class. The project features a text based pretend navigation/file management software. The templates were provided by my professor, so some of their code and comments still remain.
