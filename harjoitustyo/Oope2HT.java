/*
* Vili Kujansuu
* vili.kujansuu@tuni.fi
*/

package harjoitustyo;

/**
* Oope2HT-luokka, joka avaa käyttöliittymän. 
* <p>
* Harjoitustyö, Olio-ohjelmoinnin perusteet 2, kevät 2019.
* <p>
* @author Vili Kujansuu (vili.kujansuu@tuni.fi),
* Luonnontieteiden tiedenkunta,
* Tampereen yliopisto.
*/

public class Oope2HT {
    public static void main(String[] args) {
        // Luodaan uusi käyttöliittymä.
        KayttoLiittyma sessio = new KayttoLiittyma();
        // Avataan käyttöliittymä.
        sessio.lueSyotteet();
    }
}