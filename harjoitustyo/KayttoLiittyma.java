/*
* Vili Kujansuu
* vili.kujansuu@tuni.fi
*/

package harjoitustyo;
import harjoitustyo.tiedot.Hakemisto;
import harjoitustyo.apulaiset.*;

/**
* KayttoLiittyma-luokka, joka lukee käyttäjän syötteet ja käyttää Tulkki-luokan metodeita niiden perusteella.
* <p>
* Harjoitustyö, Olio-ohjelmoinnin perusteet 2, kevät 2019.
* <p>
* @author Vili Kujansuu (vili.kujansuu@tuni.fi),
* Luonnontieteiden tiedenkunta,
* Tampereen yliopisto.
*/

public class KayttoLiittyma {
    
    /** Luetaan käyttäjän syötteet, ja tulkitaan niitä ja käytetään Tulkki-luokan metodeja niiden perusteella. */
    public void lueSyotteet() {
        
        // Asetetaan totuusarvo sille, onko annettu poistumiskomento.
        boolean poistu = false;
        
        // Luodaan Tulkki-olio.
        Tulkki komentotulkki = new Tulkki();
        
        // Tulostetaan käynnistysviesti.
        System.out.println("Welcome to SOS.");
        
        do {
            
            // Luodaan totuusarvo, jonka avulla määritellään jatkuuko ylihakemistojen tarkasteleminen.
            boolean eiloydy = false;
            
            boolean kolmeparametria = false;
            boolean liikaaparametreja = false;
            
            // Luodaan kehotteen perusosa.
            String kehote = "/>";
            
            // Asetetaan nykyhakemiston sisällöt uuteen muuttujaan ylihakemistojen tarkastelua varten.
            Hakemisto nykyinen = komentotulkki.nykyhakemisto();
            
            // Toistetaan niin kauan kuin ylihakemistoja löytyy.
            do {
                
                // Jos ylihakemisto löytyy, niin jatketaan.
                if (nykyinen.ylihakemisto() != null) {
                    
                    // Poimitaan tarkasteltavan hakemiston nimi.
                    String nykyinenNimi = nykyinen.nimi().toString();
                    
                    // Vaihdetaan ylihakemisto tarkasteltavaksi hakemistoksi.
                    nykyinen = nykyinen.ylihakemisto();
                    
                    // Lisätään poimittu hakemiston nimi kehotteen alkuun.
                    kehote = "/" + nykyinenNimi + kehote;
                }
                else {
                    eiloydy = true;
                }
            }
            while (!eiloydy);
            
            // Tulostetaan kehote.
            System.out.print(kehote);
            
            // Luetaan käyttäjän antama komento.
            String komento = In.readString();
            
            // Luodaan omat muuttujat komennosta saaduille parametreille.
            String parametri1 = "";
            String parametri2 = "";
            String parametri3 = "";
            
            if (komento.equals("exit")) {
                System.out.println("Shell terminated.");
                poistu = true;
            }
            
            else {
        
                // Lasketaan komennossa annettujen merkkien määrä.
                int kpituus = komento.length();
            
                // Luodaan laskuri, joka laskee merkkejä 
                int laskuri = 0;
        
                // Luodaan boolean tyyppinen muuttuja, jonka totuusarvo riippuu siitä onko tutkittu merkki välilyönti.
                boolean onvali = false;
            
                char tutkittava;
        
                // Tarkastellaan komennon merkkejä kunnes komento joko loppuu tai vastaan tulee välilyönti.
                for (int i = 0; i < komento.length() && !onvali; i++) {
                    
                    // Poimitaan merkki tutkittavasta kohdasta.
                    tutkittava = komento.charAt(i);
            
                    // Lisätään laskuriin 
                    laskuri++;
            
                    // Jos tutkittava merkki ei ole välilyönti lisätään se ensimmäiseen parametriin.
                    if (tutkittava != ' ') {
                        parametri1 += tutkittava;
                    }
            
                    // Jos tutkittava merkki on välilyönti lopetetaan lisäyksien tekeminen.
                    else {
                        onvali = true;
                    }
                }
        
                // Palautetaan totuusarvo valheelliseksi, jotta voidaan tarkastella toisen parametrin osia.
                onvali = false;
        
                // Tarkastellaan komennon merkkejä uudelleen kunnes komento joko loppuu tai vastaan tulee välilyönti.
                for (int j = laskuri; j < komento.length() && !onvali; j++) {
                    
                    // Poimitaan merkki tutkittavasta kohdasta.
                    tutkittava = komento.charAt(j);
            
                    // Lisätään laskuriin.
                    laskuri++;
            
                    // Jos tutkittava merkki ei ole välilyönti lisätään se toiseen parametriin.
                    if (tutkittava != ' ') {
                        parametri2 += tutkittava;
                    }
            
                    // Jos tutkittava merkki on välilyönti lopetetaan lisäyksien tekeminen.
                    else {
                        onvali = true;
                    }
                }
        
                // Palautetaan totuusarvo valheelliseksi, jotta voidaan tarkastella kolmannen parametrin osia.
                onvali = false;
        
                // Tarkastellaan komennon merkkejä uudelleen kunnes komento joko loppuu tai vastaan tulee välilyönti.
                for (int a = laskuri; a < komento.length() && !onvali; a++) {
            
                    kolmeparametria = true;
                    
                    laskuri++;
                    
                    // Poimitaan merkki tutkittavasta kohdasta.
                    tutkittava = komento.charAt(a);
            
                    // Jos tutkittava merkki ei ole välilyönti lisätään se kolmanteen parametriin.
                    if (tutkittava != ' ') {
                        parametri3 += tutkittava;
                    }
            
                    // Jos tutkittava merkki on välilyönti lopetetaan lisäyksien tekeminen.
                    else {
                        onvali = true;
                    }
                }
                
                // Jos kolmannen parametrin määrittämisen jälkeen on vielä tekstiä, parametreja on liikaa.
                if (laskuri < komento.length()) {
                    liikaaparametreja = true;
                }
            }
            
            // Muutetaan parametrit StringBuilder muotoon 
            StringBuilder SBparametri2 = new StringBuilder(parametri2);
            StringBuilder SBparametri3 = new StringBuilder(parametri3);
            
            if (liikaaparametreja) {
                System.out.println("Error!");
            }
            
            // Jos ensimmäinen annettu parametri on md, luodaan hakemisto.
            else if (parametri1.equals("md") && !kolmeparametria && !parametri2.equals("")) {
                    
                boolean hLisatty = komentotulkki.luoHakemisto(SBparametri2);
                if (!hLisatty) {
                    System.out.println("Error!");
                }
            }
            
            // Jos ensimmäinen annettu parametri on cd, vaihdetaan hakemistoa.
            else if (parametri1.equals("cd") && !kolmeparametria) {
                boolean hVaihdettu = komentotulkki.vaihdaHakemistoa(parametri2);
                if (!hVaihdettu) {
                    System.out.println("Error!");
                }
            }
            
            // Jos ensimmäinen annettu parametri on mf, luodaan tiedosto.
            else if (parametri1.equals("mf") && kolmeparametria) {
                
                // Jos sattuu virhe, tulostetaan virheilmoitus.
                try {
                    int intparametri3 = Integer.parseInt(parametri3);
                    boolean tLisatty = komentotulkki.luoTiedosto(SBparametri2, intparametri3);
                    if (!tLisatty) {
                        System.out.println("Error!");
                    }
                }
                catch(Exception e) {
                    System.out.println("Error!");
                }
            }
            
            // Jos ensimmäinen annettu parametri on ls, haetaan toista parametria vastaavia tuloksia hakemistosta.
            else if (parametri1.equals("ls") && !kolmeparametria) {
                if (parametri2 != "") {
                    komentotulkki.listaaSisalto(parametri2);
                }
                else {
                    komentotulkki.listaaSisalto("*");
                }
            }
            
            // Jos ensimmäinen annettu parametri on rm, poistetaan toisen parametrin määrittämä tieto.
            else if (parametri1.equals("rm") && !kolmeparametria) {
                boolean poistettu = komentotulkki.poista(parametri2);
                if (!poistettu) {
                    System.out.println("Error!");
                }
            }
                
            // Jos ensimmäinen annettu parametri on mv, nimetään tieto uudelleen.
            else if (parametri1.equals("mv")) {
                boolean uNimetty = komentotulkki.nimeaUudelleen(parametri2, SBparametri3);
                if (!uNimetty) {
                    System.out.println("Error!");
                }
            }
            
            // Jos komento on tuntematon, tulostetaan virheilmoitus.
            else {
                if (!komento.equals("exit")) {
                    System.out.println("Error!");
                }
            }
            
        }
        while (!poistu);
    }
}