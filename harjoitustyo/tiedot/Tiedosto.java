/*
* Vili Kujansuu
* vili.kujansuu@tuni.fi
*/

package harjoitustyo.tiedot;
import harjoitustyo.apulaiset.*;
import java.io.*;

/**
* Tiedosto-luokka, joka sisältää tiedostolle ominaiset pirteet. Toteuttaa Syvakopioituva- ja Serializable-rajapinnat.
* Periytyy Tieto-luokasta.
* <p>
* Harjoitustyö, Olio-ohjelmoinnin perusteet 2, kevät 2019.
* <p>
* @author Vili Kujansuu (vili.kujansuu@tuni.fi),
* Luonnontieteiden tiedenkunta,
* Tampereen yliopisto.
*/

public class Tiedosto extends Tieto implements Syvakopioituva<Tiedosto>, Serializable {
      
   // Attribuutit
   
   /** Tiedoston koko int-muodossa */
   private int koko;
   
   // Rakentajat
   
   public Tiedosto() {
      koko = 0;
   }
   
   public Tiedosto(StringBuilder uusiNimi, int uusiKoko) {
      // Kutsutaan yliluokan yksiparametrillista rakentajaa.
      super(uusiNimi);
      // Kutsutaan aksessoreita, joidenka avulla tarkistetaan virheiden varalta ja määritetään tiedoston koko.
      koko(uusiKoko);
   }
   
   // Aksessorit 
   
   public int koko() {
      return koko;
   }
   
   // Lukeva aksessori heittää IllegalArgumentException tyyppisen virheen, jos annettu koko on negatiivinen.
   public void koko(int uusiKoko) throws IllegalArgumentException {
      if (0 <= uusiKoko) {
         koko = uusiKoko;
      }
      else {
         throw new IllegalArgumentException();
      }
   }
   
   // Toteutetut metodit.
   
   /** Korvataan yliluokan toString-metodi
   * @return merkkijono, joka sisältää nimen ja tiedoston koon.
   */
   // Korvataan yliluokan toString-metodi.
   @Override
   public String toString() {
      return super.toString() + " " + koko;
   }
   
   /** Korvataan yliluokan compareTo-metodi
   * @param verrattavaTieto tieto, jonka kanssa vertaus halutaan tehdä.
   * @return 1, 0, -1 riippuen minkä luvun yliluokan compareTo-metodi määrittää.
   */
   // Korvataan yliluokan compareTo-metodi.
   @Override
   public int compareTo(Tieto verrattavaTieto) {
      return super.compareTo(verrattavaTieto);
   }
   
   /** Korvataan yliluokan String-luokan equals-metodi.
   * @param verrattavaObj olio, jonka kanssa vertaus halutaan suorittaa.
   * @return true, jos vertaus onnistuu ja nimet ovat samat, false, jos vertaus epäonnistuu tai nimet eivät ole samat.
   */
   // Korvataan yliluokan String luokan equals-metodi.
   @Override
   public boolean equals(Object verrattavaObj) {
       
       // Tarkistetaan koodia virheiden varalta ja palautetaan false, jos niitä sattuu.
       try {
           // Muutetaan saatu olio Tiedosto tyyppiseksi.
           Tiedosto verrattavaTiedosto = (Tiedosto)verrattavaObj;
           
           // Käytetään yliluokan equals-metodia palauttamaan totuusarvo.
           return super.equals(verrattavaTiedosto);
       }
       catch (Exception e) {
           return false;
       }
   }
   
   /** Korvataan yliluokan Tietoinen-rajapinnan equals-metodi.
   * @param verrattavaNimi nimi, jonka kanssa
   * @return true, jos yliluokassa vertauksen tulos on true, false jos yliluokan vertauksen tulos on false.
   */
   // Korvataan yliluokan Tietoinen rajapinnan equals-metodi.
   @Override
   public boolean equals(String verrattavaNimi) {
       return super.equals(verrattavaNimi);
   }
   /** Korvataan Syvakopioituva-rajapinnan kopioi metodi ja syväkopioidaan tiedosto. 
   * @return syväkopioitu tiedosto
   */
   // Korvataan Syvakopioituva-rajapinnan kopioi-metodi. Syväkopioinnissa käytetään hyväksi Serializable rajapintaa.
   @Override
   public Tiedosto kopioi() {
       
       // Tarkistetaan tapahtuuko syväkopioinnissa virheitä. Jos tapahtuu, palautetaan null-arvo.
       try {
           
           // Luodaan tavuja taulukkoon kirjoittava virta.
           ByteArrayOutputStream tkvirta = new ByteArrayOutputStream();
           
           // Luodaan virta, joka muuttaa tavut olioiksi ja liittyy kirjoittavaan virtaan.
           ObjectOutputStream tkmvirta = new ObjectOutputStream(tkvirta);
           
           // Kirjoitetaan olio taulukkoon tavuina.
           tkmvirta.writeObject(this);
           
           // Tyhjennetään puskuri.
           tkmvirta.flush();
           
           // Suljetaan virta.
           tkmvirta.close();
           
           // Luodaan tavuja lukeva syötevirta.
           ByteArrayInputStream tlvirta = new ByteArrayInputStream(tkvirta.toByteArray());
           
           // Luodaan virta, joka muuttaa tavut olioiksi ja liittyy lukevaan virtaan.
           ObjectInputStream tlmvirta = new ObjectInputStream(tlvirta);
           
           // Luodaan kopio, joka saadaan lukemalla olion tavut taulukosta.
           Object kopioitu = tlmvirta.readObject();
           
           // Muutetaan kopioitu olio Tiedosto-muotoon ja palautetaan se.
           return (Tiedosto)kopioitu;
       }
       catch(Exception e) {
           return null;
       }
   }
}