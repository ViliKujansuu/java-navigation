/*
* Vili Kujansuu
* vili.kujansuu@tuni.fi
*/

package harjoitustyo.tiedot;
import harjoitustyo.apulaiset.*;
import java.io.*;

/**
* Abstrakti Tieto-luokka määrittelee kaikelle tiedolle yhteiset piirteet kuten nimen,
* ja korvaa joitain metodeja, jotta ne toimisivat tehtävään sopivalla tavalla.
* Toteuttaa Comparable-, Tietoinen- ja Serializable-rajapinnat.
* <p>
* Harjoitustyö, Olio-ohjelmoinnin perusteet 2, kevät 2019.
* <p>
* @author Vili Kujansuu (vili.kujansuu@tuni.fi),
* Luonnontieteiden tiedenkunta,
* Tampereen yliopisto.
*/

public abstract class Tieto implements Comparable<Tieto>, Tietoinen, Serializable {
   
   // Attribuutit
   
   /** Tiedon nimi*/
   private StringBuilder nimi;
   
   // Rakentajat
   
   public Tieto() {
      nimi = new StringBuilder("");
   }
   
   public Tieto(StringBuilder uusiNimi) {
      // Kutsutaan aksessoreita, jotka asettavat nimen ja tarkastavat syötteet oikeiksi.
      nimi(uusiNimi);
   }
   
   // Aksessorit
   
   public StringBuilder nimi() {
      return nimi;
   }
   
   /** Asettava rakentaja nimelle. Tarkistaa, että nimessä ei ole kiellettyjä merkkejä ennen asettamista 
   * @param uusiNimi tiedolle asetettava nimi
   * @throws IllegalArgumentException jos tiedolle koitetaan antaa vääränlainen nimi.
   */
   public void nimi(StringBuilder uusiNimi) throws IllegalArgumentException {
      // Tarkastetaan nimi virheiden varalta.
      if (uusiNimi != null) {
         
         // Lasketaan samojen kirjaimien määrä.
         int samat = 0;
         
         // Mitataan annetun nimen pituus.
         int nimipituus = uusiNimi.length();
         
         // Luodaan merkkijono, jossa on kaikki sallitut merkit verrattavaksi.
         String vertaus = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._";
         
         // Tarkastetaan, että löytyykö merkki listalta ja lasketaan listalla olevien merkkien määrä.
         for (int i = 0; i < nimipituus; i++) {
            char verrattava1 = uusiNimi.charAt(i);
            if (vertaus.contains(String.valueOf(verrattava1))) {
               samat++;
            }
         }
         
         // Jos samojen merkkien määrä ei ole yhtä suuri kuin alkuperäinen merkkien lukumäärä, heitetään virhe.
         // Virhe heitetään myös, jos syöte on kaksi pistettä.
         if (samat != nimipituus || uusiNimi.toString().equals("..")) {
            throw new IllegalArgumentException();
         }
         
         // Jos määrä on sama, niin asetetaan annettu merkkijono nimeksi
         else {
            nimi = uusiNimi;
         }
         
      // Jos annettu arvo on null, niin heitetään virhe.
      }
      else {
         throw new IllegalArgumentException();
      }
   }
   
   // Toteutetut metodit
   
   /** Korvataan toString metodi.
   * @return tiedon nimi muutettuna String-muotoon.
   */
   //Korvataan toString-metodi.
   @Override
   public String toString() {
      return nimi.toString();
   }
   
   /** Korvataan Comparable rajapinnan compareTo-metodi, jotta vertailu antaisi yksinkertaisempia tuloksia.
   * @param verrattavaTieto tieto, jonka kanssa vertailu tehdään.
   * @return 1, jos vertauksen tulos on positiivinen, 0, jos tulos on yhtä suuri, -1, jos tulos on negatiivinen.
   */
   // Korvataan Comparable-rajapinnan compareTo-metodi.
   @Override
   public int compareTo(Tieto verrattavaTieto) {
      
      // Muutetaan tieto StringBuilderiksi.
      StringBuilder tietoNimi = verrattavaTieto.nimi;
      
      // Muutetaan nimi merkkijonoksi.
      String mjnimi1 = nimi.toString();
      
      // Muutetaan nimi merkkijonoksi.
      String mjnimi2 = tietoNimi.toString();
      
      // Jos vertauksen tulos on negatiivinen, se on -1.
      if (mjnimi1.compareTo(mjnimi2) < 0) {
         return -1;
      }
      
      // Jos vertauksen tulos on 0, se pysyy nollana.
      else {
         if (mjnimi1.compareTo(mjnimi2) == 0) {
            return 0;
         }
         
      // Jos vertauksen tulos on positiivinen, se on 1.
      else {
         return 1;
      }
      }
   }
   
   /** Korvataan String-luokan equals-metodi. Muutetaan nimi ja verrattavan olion nimi String muotoon
   * ja verrataan niitä.
   * @param verrattavaObj olio, jonka kanssa vertailu tehdään.
   * @return totuusarvo, joka kertoo olivatko verrattattavat nimet samat.
   */
   // Korvataan String-luokan equals-metodi.
   @Override
   public boolean equals(Object verrattavaObj) {
       
       // Tarkistetaan koodia virheiden varalta ja palautetaan false, jos niitä sattuu.
       try {
           // Muutetaan verrattava olio Tieto-tyyppiseksi.
           Tieto verrattavaTieto = (Tieto)verrattavaObj;
           
           String mjnimi = nimi.toString();
           String verrattavamjnimi = verrattavaTieto.nimi.toString();
           
           // Verrataan nimiä ja palautetaan vertailun tulos.
           return mjnimi.equals(verrattavamjnimi);
       }
       catch (Exception e) {
           return false;
       }
   }
    
    /** Korvataan Tietoinen-rajapinnan equals-metodi. Otetaan vertauksessa huomioon jokerimerkit.
    * @param verrattavaNimi merkkijono, jonka kanssa vertaus tehdään.
    * @return true, jos nimi vastaa annettua merkkijonoa, false, jos se ei vastaa merkkijonoa.
    */
   // Korvataan Tietoinen rajapinnan equals-metodi niin, että vertauksessa voi käyttää jokerimerkkejä.
   @Override
   public boolean equals(String verrattavaNimi) {
      // Tarkastetaan koodia virheiden varalta ja palautetaan false, jos virheitä sattuu.
      try {
         
         // Määritellään arvot muuttujille.
         char ekamerkki = verrattavaNimi.charAt(0);
         int vertauspituus = verrattavaNimi.length();
         char vikamerkki = verrattavaNimi.charAt(vertauspituus - 1);
         String ilmantahtea = "";
         String osanvertaus = "";
         String verrattavaosa = "";
         String mjnimi = nimi.toString();
         String mjverrattavanimi = verrattavaNimi.toString();
         
         if (ekamerkki == '*' && vikamerkki != '*') {
            
            // Rakennetaan versio merkkijonosta, jossa ei ole jokerimerkkejä.
            for (int i = 1; i < vertauspituus; i++) {
               ilmantahtea += verrattavaNimi.charAt(i);
            }
            
            // Luodaan versio alkuperäisestä nimestä, joka sisältää vain merkit verrattavasta kohdasta.
            for (int b = nimi.length() - ilmantahtea.length(); b < nimi.length(); b++) {
               verrattavaosa += mjnimi.charAt(b);
            }
            
            // Verrataan jonoja ja palautetaan totuusarvo sen perusteella.
            if (verrattavaosa.equals(ilmantahtea)) {
                  return true;
               }
               else {
                  return false;
            }
         }
         
         else {
            if (ekamerkki != '*' && vikamerkki == '*') {
               
               // Rakennetaan versio merkkijonosta, jossa ei ole jokerimerkkejä.
               for (int i = 0; i < (vertauspituus - 1); i++) {
                  ilmantahtea += verrattavaNimi.charAt(i);
               }
               
               // Luodaan versio alkuperäisestä nimestä, joka sisältää vain merkit verrattavasta kohdasta.
               for (int j = 0; j < ilmantahtea.length(); j++) {
                  verrattavaosa += mjnimi.charAt(j);
               }
               
               // Verrataan jonoja ja palautetaan totuusarvo sen perusteella.
               if (verrattavaosa.equals(ilmantahtea)) {
                  return true;
               }
               else {
                  return false;
               }
            }
         
         else {
            if (ekamerkki == '*' && vikamerkki == '*') {
               
               // Rakennetaan versio merkkijonosta, jossa ei ole jokerimerkkejä.
               for (int a = 1; a < vertauspituus - 1; a++) {
                  ilmantahtea += verrattavaNimi.charAt(a);
               }
               
               // Tarkistetaan löytyykö merkkijono contains-operaation avulla ja määritetään totuusarvo.
               // Tarkistetaan myös ettei syötteenä ollut pelkkää kahta tähteä.
               if (mjnimi.contains(ilmantahtea) && !mjverrattavanimi.equals("**")) {
                  return true;
               }
               else {
                  return false;
               }
            }
         
         // Verrataan nimiä suoraan, jos jokerimerkkejä ei ole.
         else {
            if (mjnimi.equals(verrattavaNimi)) {
               return true;
            }
            else {
               return false;
            }
         }
         }
         }
      }
      catch(Exception e) {
         return false;
      }
   }
}