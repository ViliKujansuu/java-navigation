/*
* Vili Kujansuu
* vili.kujansuu@tuni.fi
*/

package harjoitustyo.tiedot;
import harjoitustyo.apulaiset.*;
import harjoitustyo.omalista.OmaLista;
import java.util.LinkedList;

/**
* Hakemisto-luokka, joka sisältää kaikki hakemistolle ominaiset piirteet.
* <p>
* Harjoitustyö, Olio-ohjelmoinnin perusteet 2, kevät 2019.
* <p>
* @author Vili Kujansuu (vili.kujansuu@tuni.fi),
* Luonnontieteiden tiedenkunta,
* Tampereen yliopisto.
*/

public class Hakemisto extends Tieto implements Sailova<Tieto> {
    
    // Attribuutit
    
    /** Hakemiston sisältö OmaLista-muodossa. */
    // Luodaan OmaLista tyyppinen attribuutti.
    private OmaLista<Tieto> sisalto;
    
    /** Hakemiston ylihakemisto */
    private Hakemisto ylihakemisto;
    
    // Rakentajat
    
    // Luodaan oletusrakentaja.
    public Hakemisto() {
        
        // Luodaan tyhjä listaolio.
        sisalto = new OmaLista<Tieto>();
        
        // Luodaan null-arvoinen hakemisto.
        ylihakemisto = null;
    }
    
    // Luodaan parametrillinen rakentaja, joka asettaa hakemiston nimen, ylihakemiston ja luo tyhjän listaolion.
    public Hakemisto(StringBuilder uusiNimi, Hakemisto uusiYlihakemisto) throws IllegalArgumentException {
        
        // Kutsutaan yliluokan yksiparametrista rakentajaa.
        super(uusiNimi);
        
        // Kutsutaan ylihakemiston aksessoreita.
        ylihakemisto(uusiYlihakemisto);
        
        // Luodaan tyhjä lista-olio.
        sisalto = new OmaLista<Tieto>();
    }
    
    // Aksessorit
    
    public OmaLista<Tieto> sisalto() {
        return sisalto;
    }
    
    public void sisalto(OmaLista<Tieto> uusiSisalto) {
        // Tarkistetaan koodi virheiden varalta ja heitetään IllegalArgumentException, jos niitä sattuu.
        try {
            // Tarkistetaan, ettei annettu sisältö ole null-arvoinen.
            if (sisalto != null) {
                sisalto = uusiSisalto;
            }
            else {
                throw new IllegalArgumentException();
            }
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
    
    public Hakemisto ylihakemisto() {
        return ylihakemisto;
    }
    
    public void ylihakemisto(Hakemisto uusiYlihakemisto) {
        // Tarkistetaan koodi virheiden varalta ja heitetään IllegalArgumentException, jos niitä sattuu.
        try {
            ylihakemisto = uusiYlihakemisto;
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
    
    // Toteutetut metodit
    
    /** Korvataan yliluokan toString-metodi lisäämäällä yliluokan toString-metodin antamaan tulokseen vielä 
    * hakemiston sisällön määrä.
    * @return merkkijono, joka kertoo hakemiston nimen ja sen sisällön määrän.
    */
    // Korvataan yliluokan toString metodi kutsumalla yliluokan toString metodia ja lisäämällä alihakemistojen määrä.
    @Override
    public String toString() {
        
        // Jos hakemistossa on sisältöä, lasketaan sen määrä ja palautetaan se hakemiston nimen kanssa.
        if (sisalto != null) {
            return super.toString() + "/ " + sisalto.size();
        }
        
        // Jos hakemistossa ei ole sisältöä palautetaan hakemiston nimi ja nolla.
        else {
            return super.toString() + "/ " + 0;
        }
    }
    
    /** Korvataan Sailova-rajapinnan hakumetodi. Etsitään hakemistosta hakusanaa vastaavia tiedostoja ja
    * asetetaan ne listaan.
    * @param hakusana hakusana, jolla haetaan vastaavia tiedostoja.
    * return lista kaikista hakusanaa vastanneista tuloksista.
    */
    // Korvataan Sailova-rajapinnan hakumetodi.
    @Override
    public LinkedList<Tieto> hae(String hakusana) {
        LinkedList<Tieto> tuloslista = new LinkedList<Tieto>();
        
        // Jatketaan vain jos sisältö ja hakusana eivät ole null-arvoisia.
        if (sisalto != null && hakusana != null) {
            
            // Totuusarvo, joka määrittää jatketaanko vertailua.
            boolean vertaillaan = true;
            
            // Tutkitaan hakemiston kaikki sisällöt ellei vertailua lopeteta kesken.
            for (int i = 0; i < sisalto.size() && vertaillaan; i++) {
                
                // Poimitaan nykyisen indeksin kohdalta tietoa tutkittavaksi.
                Tieto verrattavaTieto = sisalto.get(i);
                
                // Jos hakusana sisältää tähden, voidaan listata monta alkiota.
                if (hakusana.contains("*")) {
                
                    // Jos sisältö vastaa hakusanaa, lisätään sisältö tuloslistalle.
                    if (verrattavaTieto.equals(hakusana)) {
                        tuloslista.add(verrattavaTieto);
                    }
                }
                // Jos hakusana ei sisällä tähteä, lisätään vain yksi alkio listaan ja lopetetaan vertailu sen jälkeen.
                else {
                    
                    // Jos sisältö vastaa hakusanaa, lisätään sisältö tuloslistalle.
                    if (verrattavaTieto.equals(hakusana)) {
                        tuloslista.add(verrattavaTieto);
                        vertaillaan = false;
                    }
                }
            }
        }
        return tuloslista;
    }
    
    /** Korvataan Sailova-rajapinnan lisäysmetodi.
    * @param uusiSisalto listaan lisättävä Tieto-tyyppinen sisältö.
    * @return true, jos lisäys onnistuu, false, jos se epäonnistuu.
    */
    //Korvataan Sailova-rajapinnan lisäysmetodi.
    @Override
    // Lisätään hakemistoon sisältöä, ja palautetaan totuusarvo sen perusteella onnistuiko lisäys.
    public boolean lisaa(Tieto uusiSisalto) {
        return sisalto.lisaa(uusiSisalto);
    }
    
    /** Korvataan Sailova-rajapinnan poistometodi.
    * @param poistettava Tieto, joka halutaan poistaa hakemistosta.
    * @return true, jos jotain poistettiin, false, jos mitään ei poistettu.
    */
    // Korvataan Sailova-rajapinnan poistometodi.
    @Override
    public boolean poista(Tieto poistettava) {
        
        // Toteutetaan Ooperoiva-rajapinnan poistometodi ja asetetaan poistettujen alkioiden lukumäärä muuttujaan.
        int poistettuja = sisalto.poista(poistettava);
        
        // Jos poistettuja alkioita on, palautetaan true-arvo.
        if (0 < poistettuja) {
            return true;
        }
        
        // Jos poistettuja alkioita ei ole palautetaan false-arvo.
        else {
            return false;
        }
    }
}