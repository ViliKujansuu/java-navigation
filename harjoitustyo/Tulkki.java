/*
* Vili Kujansuu
* vili.kujansuu@tuni.fi
*/

package harjoitustyo;
import java.util.LinkedList;
import harjoitustyo.apulaiset.*;
import harjoitustyo.tiedot.*;
import harjoitustyo.omalista.*;

/**
* Tulkki-luokka, joka sisältää tarvittavat tiedot ja komennot tiedostojen ja hakemistojen hallintaan. 
* <p>
* Harjoitustyö, Olio-ohjelmoinnin perusteet 2, kevät 2019.
* <p>
* @author Vili Kujansuu (vili.kujansuu@tuni.fi),
* Luonnontieteiden tiedenkunta,
* Tampereen yliopisto.
*/

public class Tulkki {
    
    // Attribuutit
    
    /**Juurihakemisto, jonka sisälle kaikki muu lisätään.*/
    private Hakemisto juurihakemisto;
    
    /**Tällä hetkellä tarkasteltava hakemisto.*/
    private Hakemisto nykyhakemisto;
    
    // Rakentaja
    
    // Lisätään luokalle oletusrakentaja, joka asettaa null-arvon hakemistolle.
    public Tulkki() {
        juurihakemisto = new Hakemisto();
        nykyhakemisto = juurihakemisto;
    }
    
    // Aksessorit
    
    public Hakemisto juurihakemisto() {
        return juurihakemisto;
    }
    
    public void juurihakemisto(Hakemisto uusiJuurihakemisto) {
        juurihakemisto = uusiJuurihakemisto;
    }
    
    public Hakemisto nykyhakemisto() {
        return nykyhakemisto;
    }
    
    public void nykyhakemisto(Hakemisto uusiHakemisto) {
        nykyhakemisto = uusiHakemisto;
    }
    
    // Metodit
    
    /**Luodaan uusi hakemisto nykyisen hakemiston sisälle, jos hakemiston sisällä ei ole jo valmiiksi saman 
    * nimistä hakemistoa tai tiedostoa.
    * @param hakemistonimi luotavan hakemiston nimi.
    * @return true, jos hakemiston luominen onnistui, false, jos se epäonnistui.
    */
    // Metodi luo uuden hakemiston nykyisen hakemiston alahakemistoksi.
    public boolean luoHakemisto(StringBuilder hakemistonimi) {
        
        // Tarkastellaan koodia virheiden varalta, ja jos virhe sattuu palautetaan false-totuusarvo.
        try {
            // Luodaan muuttuja, joka kertoo onko tiedolla sama nimi vertailukohteen kanssa.
            boolean samanimi = false;
            
            boolean onnistuu = false;
            
            // Luodaan lista, jossa on kaikki hakemiston sisällöt.
            LinkedList<Tieto> sisaltolista = nykyhakemisto.hae("*");
        
            // Tarkistetaan kaikki listan sisällöt.
            for (int i = 0; i < sisaltolista.size(); i++) {
            
                // Poimitaan listasta tarkasteltavan tiedon nimi.
                String tulos = sisaltolista.get(i).nimi().toString();
            
                // Jos samannimisiä tietoja löytyy, hakemistoa ei voida lisätä.
                if (tulos.equals(hakemistonimi.toString())) {
                    samanimi = true;
                }
            }
        
            // Jos saman nimisiä hakemistoja tai tiedostoja ei löydy, lisätään hakemisto.
            if (!samanimi) {
            
                Hakemisto uusiHakemisto = new Hakemisto(hakemistonimi, nykyhakemisto);
                nykyhakemisto.lisaa(uusiHakemisto);
            
                // Asetetaan onnistumisen totuusarvoksi true, lisäyksen onnistuessa.
                onnistuu = true;
            }
            
            // Palautetaan tieto lisäyksen onnistumisesta.
            return onnistuu;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    /**Metodi luo uuden tiedoston nykyiseen hakemistoon, jos saman nimistä hakemistoa tai samanlaista tiedostoa
    * ei ole jo hakemistossa.
    * @param tiedostonimi lisättävän tiedoston nimi.
    * @param koko lisättävän tiedoston koko.
    * @return true, jos tiedoston lisääminen onnistui, false, jos se epäonnistui.
    */
    // Metodi luo uuden tiedoston nykyiseen hakemistoon.
    public boolean luoTiedosto(StringBuilder tiedostonimi, int koko) {
        
        // Tarkistetaan koodia virheiden varalta, ja jos virhe sattuu, palautetaan totuusarvo false.
        try {
            // Luodaan muuttuja, joka kertoo onko tiedolla sama nimi vertailukohteen kanssa.
            boolean samanimi = false;
            
            boolean onnistuu = false;
        
            // Luodaan lista, jossa on kaikki hakemiston sisällöt.
            LinkedList<Tieto> sisaltolista = nykyhakemisto.hae("*");
        
            // Tarkastetaan listan jokainen kohta, yhteensopimattoman tiedon varalta.
            for (int i = 0; i < sisaltolista.size(); i++) {
            
                // Poimitaan vertailuun tarvittava tieto taulukosta ja muutetaan sen nimi merkkijonoksi.
                Tieto tulos = sisaltolista.get(i);
                String tulosjono = tulos.nimi().toString();
            
                // Luodaan vertailuun tarvittava jono.
                String vertailujono = tiedostonimi.toString();
            
                // Jos tiedot ovat samat vaihdetaan totuusarvo trueksi.
                if (tulosjono.equals(vertailujono)) {
                    samanimi = true;
                }
            }
        
            // Jos ei ole hakemistoa, jolla on sama nimi tai täsmälleen samanlaista tiedostoa, lisätään tiedosto.
            if (!samanimi) {
                
                Tiedosto uusiTiedosto = new Tiedosto(tiedostonimi, koko);
                onnistuu = nykyhakemisto.lisaa(uusiTiedosto);
            }
            
            return onnistuu;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    /**Metodi listaa nykyhakemiston hakusanaa vastaavan sisällön.
    * @param hakusana hakusana, jota vastaavaa sisältöä etsitään. Voi sisältää jokerimerkkejä.
    */
    // Metodi listaa nykyhakemiston hakusanaa vastaavan sisällön.
    public void listaaSisalto(String hakusana) {
        
        // Luodaan lista, jossa on kaikki hakusanaa vastaavat tulokset.
        LinkedList<Tieto> tuloslista = nykyhakemisto.hae(hakusana);
        
        // Jos hakusanaa ei ole tai hakusanalla löytyy tuloksia, tulostetaan listan sisällöt.
        if (0 < tuloslista.size() || hakusana.equals("*")) {
            for (int i = 0; i < tuloslista.size(); i++) {
                String loydetty = tuloslista.get(i).toString();
                System.out.println(loydetty);
            }
        }
        // Jos hakusanalla ei löydy tuloksia tulostetaan virheilmoitus.
        else {
            System.out.println("Error!");
        }
    }
    
    /**Metodi nimeää tiedoston tai hakemiston uudelleen. Tietoa ei voi nimetä uudelleen samannimiseksi kuin
    * samassa hakemistossa oleva tiedosto tai hakemisto.
    * @param vanhaNimi uudelleen nimettävän kohteen nimi.
    * @param uusiNimi kohteelle annettava uusi nimi.
    * @return true, jos nimen muuttaminen onnistuu, false, jos se epäonnistuu.
    */
    // Metodi nimeää tiedoston tai hakemiston uudelleen.
    public boolean nimeaUudelleen(String vanhaNimi, StringBuilder uusiNimi) {
        
        // Tarkistetaan koodia virheiden varalta.
        try {
            
            boolean onnistuu = false;
            
            // Muutetaan uusi nimi String tyyppiseksi.
            String uusinimijono = uusiNimi.toString();
            
            // Luodaan muuttuja, johon tallennetaan tieto siitä löytyykö uusi nimi jo hakemistosta.
            boolean uusiloytyy = false;
            
            // Luodaan muuttuja, jonka avulla tarkistetaan sisältääkö uusi nimi sallittuja merkkejä.
            boolean sallittu = false;
            
            // Merkkijono, joka sisältää sallitut merkit.
            String vertaus = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789._";
            
            // Laskuri sallituille merkeille.
            int verrattuja = 0;
            
            // Verrataan nimen merkkejä sallittuihin merkkeihin ja lisätään laskuriin, jos merkki on sallittu.
            for (int a = 0; a < uusinimijono.length(); a++) {
                char verrattava1 = uusinimijono.charAt(a);
                for (int b = 0; b < vertaus.length(); b++) {
                    char verrattava2 = vertaus.charAt(b);
                    if (verrattava1 == verrattava2) {
                        verrattuja++;
                    }
                }
            }
            
            // Jos kaikki merkit ovat sallittuja annetaan sallittu-muuttujalle totuusarvoksi true.
            if (verrattuja == uusinimijono.length()) {
                sallittu = true;
            }
            
            // Tarkistetaan ettei hakemistosta löydy tietoa, joka vastaa uutta nimeä.
            for (int j = 0; j < nykyhakemisto.sisalto().size(); j++) {
                String uuteenverrattava = nykyhakemisto.sisalto().get(j).nimi().toString();
                if (uuteenverrattava.equals(uusinimijono)) {
                    uusiloytyy = true;
                }
            }
            
            // Tarkistetaan nykyisen hakemiston jokainen kohta.
            for (int i = 0; i < nykyhakemisto.sisalto().size(); i++) {
                
                // Poimitaan tietoa tarkasteltavaksi nykyisestä kohdasta. 
                Tieto verrattavatieto = nykyhakemisto.sisalto().get(i);
                
                // Asetetaan tarkastellun kohdan nimi muuttujaan.
                String verrattavanimi = verrattavatieto.nimi().toString();
                
                // Jos löydetty nimi on sama kuin etsitty nimi, korvataan tiedosto uudelleen nimetyllä versiolla.
                if (verrattavanimi.equals(vanhaNimi) && !uusiloytyy && sallittu) {
                    
                    // Poistetaan Tieto, jolla on vanhanimi.
                    poista(vanhaNimi);
                    
                    // Uudelleen nimetään löydetty tieto.
                    verrattavatieto.nimi(uusiNimi);
                    
                    // Lisätään uudelleen nimetty tieto takaisin hakemistoon.
                    nykyhakemisto.lisaa(verrattavatieto);
                    onnistuu = true;
                }
            }
            
            return onnistuu;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    /**Poistetaan merkkijonoa vastaavat tiedot hakemistosta.
    * @param poistettava merkkijono, joka määrittää mitkä tiedostot poistetaan.
    * @return true, jos poistaminen onnistuu, false, jos se epäonnistuu.
    */
    // Metodi poistaa sanaa vastaavat tiedot hakemistosta.
    public boolean poista(String poistettava) {
        try {
            // Luodaan lista, joka sisältää kaikki nykyhakemiston tiedot.
            LinkedList<Tieto> tuloslista = nykyhakemisto.hae("*");
        
            // Luodaan muuttuja, joka kertoo onko jotain poistettu.
            boolean poistettu = false;
        
            // Tarkistetataan jokainen hakemiston kohta.
            for (int i = 0; i < tuloslista.size(); i++) {
            
                // Poimitaan jokaisesta kohdasta tietoa verrattavaksi.
                Tieto verrattavaTieto = tuloslista.get(i);
            
                // Jos verrattava tieto vastaa poistamiskäskyä, poistetaan tieto.
                if (verrattavaTieto.equals(poistettava)) {
                    poistettu = nykyhakemisto.poista(verrattavaTieto);
                }
            }
        
            // Palautetaan tulos siitä, että poistettiinko mitään.
            return poistettu;
        }
        catch (Exception e) {
            return false;
        }
    }
    
    /**Vaihdetaan nykyhakemistoa yli- tai alihakemistoon.
    * @param hakemistoNimi sen hakemiston nimi, johon sijaintia halutaan vaihtaa.
    * @return true, jos hakemiston vaihtaminen onnistui, false, jos se epäonnistui.
    */
    // Metodi vaihtaa nykyhakemistoa.
    public boolean vaihdaHakemistoa(String hakemistoNimi) {
        
        try {
            
            // Luodaan lista hakemiston sisällöistä.
            LinkedList<Tieto> tuloslista = nykyhakemisto.hae(hakemistoNimi);
            
            // Luodaan muuttuja, johon liitetään tieto etsityn hakemiston löytymisestä.
            boolean loytyy = false;
            
            // Jos syötteenä ollaan annettu kaksi pistettä, vaihdetaan ylihakemistoon.
            if (hakemistoNimi.equals("..")) {
                if (nykyhakemisto.ylihakemisto() != null) {
                    nykyhakemisto = nykyhakemisto.ylihakemisto();
                    loytyy = true;
                }
            }
            else if (hakemistoNimi.equals("")){
                nykyhakemisto = juurihakemisto;
                loytyy = true;
            }
        
            // Jos syötteenä on jotain muuta, tehdään seuraavasti.
            else {
                // Käydään kaikki hakemiston sisällöt läpi.
                for (int i = 0; i < tuloslista.size(); i++) {
                
                    // Poimitaan tietoa verrattavaksi.
                    Tieto verrattavaTieto = tuloslista.get(i);
            
                    // Tarkastetaan vastaako poimitto tieto hakemiston nimeä.
                    if (verrattavaTieto.equals(hakemistoNimi)) {
                    
                        // Tarkistetaan että kyseessä on hakemisto.
                        if (verrattavaTieto.toString().contains("/")) {
                    
                            // Vaihdetaan nykyisen hakemiston arvo poimituksi hakemistoksi.
                            nykyhakemisto = (Hakemisto)verrattavaTieto;
                            loytyy = true;
                        }
                    }
                }
            }
            
            // Palautetaan tieto siitä löytyikö hakemisto, johon halutaan siirtyä.
            return loytyy;
        }
        catch(Exception e) {
            return false;
        }
    }
}