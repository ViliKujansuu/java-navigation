/*
* Vili Kujansuu
* vili.kujansuu@tuni.fi
*/

package harjoitustyo.omalista;
import harjoitustyo.apulaiset.*;
import java.util.LinkedList;

/**
* OmaLista-luokka, joka sisältää operaatiot hakemistojen ja tiedostojen listaamiseen. Periytyy LinkedList luokasta
* ja soveltaa Ooperoiva rajapintaa.
* <p>
* Harjoitustyö, Olio-ohjelmoinnin perusteet 2, kevät 2019.
* <p>
* @author Vili Kujansuu (vili.kujansuu@tuni.fi),
* Luonnontieteiden tiedenkunta,
* Tampereen yliopisto.
*/

public class OmaLista<E> extends LinkedList<E> implements Ooperoiva<E> {
    
    /** Lisätään tietoa listaan ja järjestetään lista niin, että tieto asettuu oikeaan järjestykseen
    * @param uusi listaan lisättävä sisältö.
    * @return true, jos lisääminen onnistui, false, jos se epäonnistui.
    */
    @Override
    @SuppressWarnings({"unchecked"})
    public boolean lisaa(E uusi) {
        // Tarkistetaan koodia virheiden varalta.
        try {
            
            // Tehdään testivertaus, jonka perusteella tarkistetaan pystyykö muuttuja toteuttamaan compareTo-metodin.
            Comparable testivertaus = (Comparable)uusi;
            if (testivertaus.compareTo(testivertaus) == 0) {
                
                // Jos testivertaus onnistuu, lisätään muuttuja listaan.
                addFirst(uusi);
            }
            
            // Vertaillaan jokaista päällekkäistä kohtaa ja vaihdetaan ne, kun on tarpeellista.
            for (int i = 0; i < size() - 1; i++) {
                
                // Otetaan alkiot päällekkäisitä kohdista.
                E alkio1 = get(i);
                E alkio2 = get(i + 1);
                
                // Muutetaan alkiot Comparable-muotoon.
                Comparable verrattava1 = (Comparable)alkio1;
                Comparable verrattava2 = (Comparable)alkio2;
                
                // Tehdään vertaus, ja jos ylempi alkio on suurempi, vaihdetaan niiden paikat.
                if (0 <= verrattava1.compareTo(verrattava2)) {
                    set(i, alkio2);
                    set(i + 1, alkio1);
                }
                
            }
            // Palautetaan totuusarvo true, jos lisäys onnistuu.
            return true;
        }
        // Jos virheitä sattuu palautetaan totuusarvo false.
        catch(Exception e) {
            return false;
        }
    }
    
    /** Poistetaan listalta halutut kohdat.
    * @param poistettava sisältö, joka halutaan poistaa.
    * @return poistettujen kohteiden määrä.
    */
    @Override
    // Korvataan Ooperoiva-rajapinnan metodi alkion poistamiselle.
    public int poista(E poistettava) {
        
        // Luodaan muuttuja johon, lasketaan poistettujen alkioiden määrä.
        int poistettuja = 0;
        
        // Käydään koko lista läpi yksitellen.
        for (int i = 0; i < size(); i++) {
            
            // Asetetaan haluttu alkio muuttujaan.
            E vertaus = get(i);
            
            // Tarkistetaan onko listan alkio sama kuin poistettava.
            if (poistettava == vertaus) {
                
                // Poistetaan alkio, lisätään poistokerta laskuriin ja vähennetään indeksiä yhdellä.
                remove(i);
                poistettuja++;
                i--;
            }
        }
        // Palautetaan poistettujen alkioiden määrä.
        return poistettuja;
    }
}